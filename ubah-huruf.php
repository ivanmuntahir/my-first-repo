<?php
function ubah_huruf($str)
{
//kode di sini
	$huruf = "abcdefghijklmnopqrstuvwxyz";
	$temp = '';

	for ($p = 0; $p < strlen($str); $p++){
		$pos = strrpos($huruf, $str[$p]);
		$temp .= substr($huruf, $pos + 1, 1);
	}
	return $temp;
}

// TEST CASES
echo ubah_huruf("wow"); 
echo ("<br>");// xpx
echo ubah_huruf('developer');
echo ("<br>"); // efwfmpqfs
echo ubah_huruf('laravel'); 
echo ("<br>");// mbsbwfm
echo ubah_huruf('keren');
echo ("<br>"); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
echo ("<br>");
?>